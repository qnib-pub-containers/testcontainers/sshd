# syntax=docker/dockerfile:1.4
FROM alpine:3.9
LABEL maintainer "Adrian B. Danieli - https://github.com/sickp"
ENTRYPOINT ["/entrypoint.sh"]
EXPOSE 22
COPY entrypoint.sh /

RUN apk add --no-cache openssh \
  && sed -i s/#PermitRootLogin.*/PermitRootLogin\ yes/ /etc/ssh/sshd_config \
  && echo "root:root" | chpasswd

RUN <<eot ash
 mkdir -p /root/.ssh
 chmod 0700 /root/.ssh
 passwd -u root
 ssh-keygen -f ~/.ssh/id_rsa -N ""
 chmod 0600 /root/.ssh/id_rsa
 chmod 0640 /root/.ssh/id_rsa.pub
 cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
 chmod 0600 /root/.ssh/authorized_keys
eot